package com.simplilearn.flight.flyaway;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;


import com.simplilearn.flight.flyaway.entity.Passenger;
import com.simplilearn.flight.flyaway.entity.dao.PassengerDAO;


@Path("/Passengers")
public class PassengerResource {
	@GET
    @Produces("application/json")
    public List<Passenger> getPassenger() {
        PassengerDAO dao = new PassengerDAO();
        List Passengers = dao.getPassengers();
        return Passengers;
    }
 
    
    @POST
    @Path("/create")
    @Consumes("application/json")
    
   
    
    
   public Response addAirport(Passenger passenger){
    	passenger.setFirstName(passenger.getFirstName());
    	passenger.setLastName(passenger.getLastName());
    	passenger.setEmail(passenger.getEmail());
        PassengerDAO dao = new PassengerDAO();
        dao.addPassenger(passenger);
        return Response.ok().build();
        
        
    }
    @PUT
    @Path("/update/{2}")
    @Consumes("application/json")
    public Response updatePassenger(@PathParam("id") int id, Passenger passenger){
        PassengerDAO dao = new PassengerDAO();
        int count = dao.updatePassenger(id, passenger);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
    
    @DELETE
    @Path("/{3}")
    @Consumes("application/json")
    public Response deleteAirport(@PathParam("id") int id){
        PassengerDAO dao = new PassengerDAO();
        int count = dao.deletePassenger(id);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
}




