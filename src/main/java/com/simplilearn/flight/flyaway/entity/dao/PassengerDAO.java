package com.simplilearn.flight.flyaway.entity.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.simplilearn.flight.flyaway.entity.Passenger;
import com.simplilearn.flight.flyaway.entity.util.SessionUtil;

public class PassengerDAO {
	
	public void addPassenger(Passenger bean){
        Session session = SessionUtil.getSession();        
        Transaction tx = session.beginTransaction();
        addPassenger(session,bean);        
        tx.commit();
        session.close();
        
    }
    
	private void addPassenger(Session session, Passenger bean){
		Passenger passenger = new Passenger();
		passenger.setId(bean.getId());
		passenger.setFirstName(bean.getFirstName());
		passenger.setLastName(bean.getLastName());
		passenger.setEmail(bean.getEmail());
		session.save(passenger); 
    	}
    
    public List<Passenger> getPassengers(){
        Session session = SessionUtil.getSession();    
        Query query = session.createQuery("from Passenger");
        List<Passenger> Passengers =  query.list();
        session.close();
        return Passengers;
    }
 
    public int deletePassenger(int id) {
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        String hql = "delete from Passenger where id = :id";
        Query query = session.createQuery(hql);
        query.setInteger("id",id);
        int rowCount = query.executeUpdate();
        System.out.println("Rows affected: " + rowCount);
        tx.commit();
        session.close();
        return rowCount;
    }
    
    public int updateAirport(int id, Passenger passenger){
         if(id <=0)  
               return 0;  
         Session session = SessionUtil.getSession();
            Transaction tx = session.beginTransaction();
            String hql = "update Airport set id = :id, firstname=:firstname, lastname:=lastname, email:=email where id = :id";
            Query query = session.createQuery(hql);
            
            query.setInteger("id",id);
            query.setString("id",passenger.getId());
            query.setString("firstname",passenger.getFirstName());
            query.setString("lastname",passenger.getLastName());
            query.setString("email",passenger.getEmail());
            int rowCount = query.executeUpdate();
            System.out.println("Rows affected: " + rowCount);
            tx.commit();
            session.close();
            return rowCount;
    }
}



